

var chai= require('chai');
var chaiHttp= require('chai-http');
var	app='http://localhost:3000';
var expect = chai.expect;

	chai.use(chaiHttp);
	chai.should();

describe('BreastCancers Api testing (LOGS)', function(){
	var loginfo={};
  var userinfo={};
  var page=[];
  var logTranType=[];
  var data={};

   describe('POST', function(){
    it('function should post user information to db to use the userID for logs sample post', function(done){
      var userinformation={u_lname:'examplelname',u_fname:'examplefname',organization:'forloop',u_email:'example@example.com',u_password:'123456789'};
    chai.request(app)
        .post('/api/1.0/user')
        .send(userinformation)
        .end(function (err,res) {
          expect(res.body.u_lname).to.equal('examplelname');
          expect(res.body.u_fname).to.equal('examplefname');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          userinfo['userID']=res.body.userID;
          done();
          });

    })
  })
 describe('POST', function(){
    it('function should post logs information to db with parameter userID', function(done){
    	var pagename={name:'sample page name'};
      var logtrantydesc={desc:'sample logtrantype desc'};
      page.push(pagename);
      logTranType.push(logtrantydesc);
         data['page']=page;
         data['logTranType']=logTranType;
		chai.request(app)
			  .post('/api/1.0/log/'+userinfo['userID'])
			  .send(data)
			  .end(function (err,res) {
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
          loginfo['logID']=res.body._id;
    			done();
  				});

    })
  })
 describe('GET', function(){
    it('function should get all log information by the user with parameter userID from db', function(done){
    chai.request(app)
        .get('/api/1.0/log/'+userinfo['userID'])
        .end(function (err,res) {
           for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('DateTran');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
  describe('GET', function(){
    it('function should get all log information from db', function(done){
    chai.request(app)
        .get('/api/1.0/log/')
        .end(function (err,res) {
          for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('DateTran');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
  describe('Delete', function(){
    it('function should delete user info used from testing', function(done){
      
    chai.request(app)
        .delete('/api/1.0/user/'+userinfo['userID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
  describe('Delete', function(){
    it('function should delete log info used from testing', function(done){
      
    chai.request(app)
        .delete('/api/1.0/log/'+loginfo['logID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
   
})

