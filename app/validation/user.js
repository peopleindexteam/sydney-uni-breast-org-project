'use strict';

exports.validateUser = function(req, res, next) {

    req.checkBody('u_email', 'Please enter a Email').notEmpty();
    req.checkBody('u_email', 'Email address needs to be in the format yourname@domain.com.').isEmail();
    req.checkBody('u_password', 'Please enter a Password').notEmpty();
    req.checkBody('u_password', '6 to 10 characters required').len(6, 10);
    req.checkBody('u_lname', 'Please enter an Last Name').notEmpty();
    req.checkBody('u_fname', 'Please enter your First Name').notEmpty();
    req.checkBody('organization', 'Please enter you Organizaton').notEmpty();

    var errors = req.validationErrors();

    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};
