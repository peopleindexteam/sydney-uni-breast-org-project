

var chai= require('chai');
var chaiHttp= require('chai-http');
var	app='http://localhost:3000';
var expect = chai.expect;

	chai.use(chaiHttp);
	chai.should();

describe('BreastCancers Api testing For (ROLE)', function(){
	var roleinfo={};

 describe('POST', function(){
    it('function should post role information to db', function(done){
    	var roleinformation={RoleDesc:'administrator',Read:1,Write:1,Delete:1};
		chai.request(app)
			  .post('/api/1.0/roles')
			  .send(roleinformation)
			  .end(function (err,res) {
			  	expect(res.body.RoleDesc).to.equal('administrator');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
          roleinfo['roleID']=res.body._id;
    			done();
  				});

    })
  })
   describe('GET', function(){
    it('function should get role information with parameter roleID from db', function(done){
		chai.request(app)
			  .get('/api/1.0/roles/'+roleinfo['roleID'])
			  .end(function (err,res) {
			  	expect(res.body.RoleDesc).to.equal('administrator');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
    			done();
  				});

    })
  })
   describe('PUT', function(){
    it('function should update Role Information', function(done){
    	var updateroleinformation={RoleDesc:'Guest',Read:0,Write:0,Delete:0};
		chai.request(app)
			  .put('/api/1.0/roles/'+roleinfo['roleID'])
			  .send(updateroleinformation)
			  .end(function (err,res) {
			    expect(res.body.RoleDesc).to.equal('Guest');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
    			done();

  				});

    })
  })
    describe('GET', function(){
    it('function should get all role information  from db', function(done){
    chai.request(app)
        .get('/api/1.0/roles/')
        .end(function (err,res) {
          for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('RoleDesc');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
 
    describe('Delete', function(){
    it('function should delete role from db with parameter roleID', function(done){
		chai.request(app)
			  .delete('/api/1.0/roles/'+roleinfo['roleID'])
			  .end(function (err,res) {
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
    			done();

  				});

    })
  })
})

