'use strict';

var async = require('async');
var mongoose = require('mongoose');
var country = mongoose.model('country');
var state = mongoose.model('state');
var location = mongoose.model('location');


// Save data
exports.saveCountry = function saveCountry(data,next){
	country.create(data, next);
};

exports.getallCountry= function getallCountry(next){
	country.find(next);
};
exports.getSPcountry = function getSPcountry(id,next){
	country.find({_id:id},next);
}

exports.updateCountry = function updateCountry(_id,data, next) {
    country.findByIdAndUpdate(_id,data, next);
};
exports.delCountryByID = function delCountryByID(id,next){
	country.findByIdAndRemove(id,next);
}

exports.saveState = function saveState(id,data, next) {
	data['countryID'] = id;
	state.create(data,next);
};
exports.getstates = function getstates(next){
	state.find(next);
}

exports.getallState = function getallState(id,next){
	state.find({countryID: id},next);
};

exports.getspec_State = function getspec_State(id,next){
	state.find({_id:id},next);
};

exports.updateState = function updateState(_id,data, next) {
    state.findByIdAndUpdate(_id,data, next);
};

exports.deleteStateByID = function deleteStateByID(id,next){
    state.findByIdAndRemove(id,next);
};

exports.saveLocation= function saveLocation(idc,ids,data,next){
	data['countryID'] = idc;
	data['stateID']= ids;
	location.create(data,next);
};
exports.getLocations= function getLocations(next){
	location.find(next);
};

exports.getallLocation = function getallState(id,next){
	location.find({stateID: id},next);
};

exports.getspecificlocation=function getspecificlocation(id,next){
	location.find({_id: id},next);
};

exports.updateLocation = function updateLocation(id,data,next){
	location.findByIdAndUpdate(id,data,next);
};

exports.deleteSpecificLocationByID = function deleteSpecificLocationByID(id,next){
	location.findByIdAndRemove(id,next);
}

exports.getallAddress=function getallAddress(next){
    location.find()
    .populate('stateID')
    .populate('countryID')
    .exec(next);
};

