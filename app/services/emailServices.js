'use strict';

var config = require('../../config/environment/development');

var async = require('async');

var mongoose = require('mongoose');
var User = mongoose.model('User_Account');

var mailgun = require('mailgun-js')({
    apiKey: config.mailgun_api_key,
    domain: config.mailgun_domain
});

exports.emailVerification = function (data,callback) {
    console.log(data);
    var email = {
        from: 'support@breastcancer.com',
        to: data,
        subject: 'Email Verification',
        text: 'This is sample Email Verification'
    };
    mailgun.messages().send(email, function (err, body) {
        if (err) {
             return callback(err,null);
        } else {
            return callback(null, {return: true});
        }
    });
}

exports.recoverPassword = function (data,next) {
    var sender = data.email;
    var new_pass = generatePassword(8);

    async.waterfall([
        function (callback) {
            User.findOneAndUpdate({
                u_email: sender
            }, {
                $set: {
                    'u_password': new_pass
                }
            }, function (error, data) {
                return callback(error, data);
            });
        },
        function (data, callback) {
            console.log(data);

            var email = {
                from: 'support@breastcancer.com',
                to: data.u_email,
                subject: 'General Bitcoin Password Recovery',
                text: 'General Bitcoin Password Recovery.\n\n Your new password is ' + data.u_password
            };
            mailgun.messages().send(email, function (err, body) {
                if (err) {
                    console.error(err);
                    return callback(err,null);
                } else {
                    return callback(null, {return: true});
                }
            });
        }
    ], next);
}



function generatePassword(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
