'use strict';


var logDao = require('../daos/logDao');

 function Log() {
    this.logDao = logDao;
 }

 Log.prototype.savelogs = function(id,data, next) {
     logDao.savelogs(id,data, next);
 };

 Log.prototype.getalllogs= function(next){
     logDao.getalllogs(next);
 };

 Log.prototype.findlogs = function(id,next){
     logDao.findlogs(id,next);
 };

 Log.prototype.deleteLogsByUserId = function(id, next){
 	logDao.deleteLogsByUserId(id, next);
 }

exports.Log = Log;
