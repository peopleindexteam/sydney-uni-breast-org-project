'use strict';


var mongoose = require('mongoose');
var roles = mongoose.model('Roles');


// Save data
exports.saverole = function saverole(data, next) {
    roles.create(data, next);
};

exports.getallrole=function getallrole(next){
    roles.find(next);
};

exports.updaterole = function updaterole(_id,data, next) {
    roles.findByIdAndUpdate(_id,data, next);
};

exports.findrole = function findrole(id, next) {
    roles.findById(id, next);
};

exports.deleteRolesById = function deleteRolesById(id, next){
	roles.findByIdAndRemove(id, next);
};
