'use strict';


var addressDao = require('../daos/addressDao');

 function Address() {
    this.addressDao = addressDao;
 }
Address.prototype.saveCountry = function(data,next){
	addressDao.saveCountry(data, next);
}
Address.prototype.getallCountry = function(next){
	addressDao.getallCountry(next);
}
Address.prototype.getSPcountry = function(id,next){
	addressDao.getSPcountry(id,next);
}
Address.prototype.updateCountry = function(id,data,next){
	addressDao.updateCountry(id,data,next);
}
Address.prototype.delCountryByID = function(id,next){
	addressDao.delCountryByID(id,next);
}
Address.prototype.saveState = function(id,data,next){

	addressDao.saveState(id,data, next);
}
Address.prototype.getspec_State=function(id,next){
	addressDao.getspec_State(id,next);
}
Address.prototype.getstates = function(next){
	addressDao.getstates(next);
}
Address.prototype.getallState = function(id,next){
	addressDao.getallState(id,next);
}
Address.prototype.updateState = function(id,data,next){
	addressDao.updateState(id,data,next);
}
Address.prototype.deleteStateByID = function(id,next){
	addressDao.deleteStateByID(id,next);
}
Address.prototype.saveLocation = function(idc,ids,data,next){
	addressDao.saveLocation(idc,ids,data, next);
}
Address.prototype.getLocations = function(next){
	addressDao.getLocations(next);
}
Address.prototype.getallLocation = function(id,next){
	addressDao.getallLocation(id,next);
}
Address.prototype.updateLocation = function(id,data,next){
	addressDao.updateLocation(id,data,next);
}
Address.prototype.getspecificlocation =function(id,next){
	addressDao.getspecificlocation(id,next);
}
Address.prototype.deleteSpecificLocationByID = function(id,next){
	addressDao.deleteSpecificLocationByID(id,next);
}
 Address.prototype.getallAddress= function(next){
     addressDao.getallAddress(next);
 };
exports.Address = Address;
