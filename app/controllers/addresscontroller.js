'use strict';

var addressService = require('../services/addressService').Address;
var address = new addressService();

var cb = require('./../utils/callback');


exports.saveCountry = function onRequest(req, res) {
    address.saveCountry(req.body, cb.setupResponseCallback(res));
};

exports.getCountry = function onRequest(req, res, next) {
    address.getallCountry(cb.setupResponseCallback(res));
}

exports.getCountryById = function onRequest(req, res, next) {
    address.getSPcountry(req.params.id, cb.setupResponseCallback(res));
}

exports.updateCountryById = function onRequest(req, res, next) {
    address.updateCountry(req.params.id, req.body, cb.setupResponseCallback(res));
}

exports.delCountryByID = function onRequest(req,res,next){
    address.delCountryByID(req.params.id,cb.setupResponseCallback(res));
}



exports.getState = function onRequest(req, res, next) {
    address.getstates(cb.setupResponseCallback(res));
}

exports.saveStateByCountryId = function onRequest(req, res, next) {
    address.saveState(req.params.id, req.body, cb.setupResponseCallback(res));
}

exports.getStateByCountryId = function onRequest(req, res, next) {
    address.getallState(req.params.id, cb.setupResponseCallback(res));
}

exports.getStateById = function onRequest(req, res, next) {
    address.getspec_State(req.params.id, cb.setupResponseCallback(res));
}

exports.updateStateById = function onRequest(req, res, next) {
    address.updateState(req.params.id, req.body, cb.setupResponseCallback(res));
}
exports.deleteStateByID = function onRequest(req,res,next){
    address.deleteStateByID(req.params.id,cb.setupResponseCallback(res));
}

exports.saveLocationByCountryIdStateId = function onRequest(req, res, next) {
    address.saveLocation(req.params.idc, req.params.ids, req.body, cb.setupResponseCallback(res));
}




exports.getLocation = function onRequest(req, res, next) {
    address.getLocations(cb.setupResponseCallback(res));
}

exports.getLocationById = function onRequest(req, res, next) {
    address.getallLocation(req.params.id, cb.setupResponseCallback(res));
}

exports.getSpecificLocationById = function onRequest(req, res, next) {
    address.getspecificlocation(req.params.id, cb.setupResponseCallback(res));
}

exports.updateSpecificLocationById = function onRequest(req, res, next) {
    address.updateLocation(req.params.id, req.body, cb.setupResponseCallback(res));
}
exports.deleteSpecificLocationByID = function onRequest(req,res,next){
    address.deleteSpecificLocationByID(req.params.id,cb.setupResponseCallback(res));
}


exports.getAddress = function onRequest(req, res, next) {
    address.getallAddress(cb.setupResponseCallback(res));
}
