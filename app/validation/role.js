'use strict';

exports.validateRoles = function(req, res, next){
	req.checkBody('RoleDesc','Please enter a Role Description').notEmpty();
	req.checkBody('Read', 'Invalid format it should be Integer').isInt();
	req.checkBody('Write', 'Invalid format it should be Integer').isInt();
	req.checkBody('Delete', 'Invalid format it should be Integer').isInt();

	var errors = req.validationErrors();

    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
}