

var chai= require('chai');
var chaiHttp= require('chai-http');
var	app='http://localhost:3000';
var expect = chai.expect;

	chai.use(chaiHttp);
	chai.should();

describe('BreastCancers Api testing For (ADDRESS) ', function(){

	var countryinfo={};
  var stateinfo={};
  var locationinfo={};

//country data
 describe('POST', function(){
    it('function should post Country information to db', function(done){
    	var countryinformation={c_name:'sample country',c_Desc:"sample description"};
		chai.request(app)
			  .post('/api/1.0/country')
			  .send(countryinformation)
			  .end(function (err,res) {
			  	expect(res.body.c_name).to.equal('sample country');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
          countryinfo['countryID']=res.body._id;
    			done();
  				});

    })
  })
 describe('GET', function(){
    it('function should get country information with parameter countryID from db', function(done){
    chai.request(app)
        .get('/api/1.0/country/'+countryinfo['countryID'])
        .end(function (err,res) {
          expect(res.body[0].c_name).to.equal('sample country');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
  describe('GET', function(){
    it('function should get all country information  from db', function(done){
    chai.request(app)
        .get('/api/1.0/country/')
        .end(function (err,res) {
          for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('c_name');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
  describe('PUT', function(){
    it('function should update Country Information', function(done){
     var updatecountryinformation={c_name:'sample country update',c_Desc:"sample description update"};
    chai.request(app)
        .put('/api/1.0/country/'+countryinfo['countryID'])
        .send(updatecountryinformation) 
        .end(function (err,res) {
          expect(res.body.c_name).to.equal('sample country update');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
//state data
  describe('POST', function(){
    it('function should post state information to db parameter countryID', function(done){
      var stateinformation={s_name:'sample state',s_code:"1234"};
    chai.request(app)
        .post('/api/1.0/state/'+countryinfo['countryID'])
        .send(stateinformation)
        .end(function (err,res) {
          expect(res.body.s_name).to.equal('sample state');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          stateinfo['stateID']=res.body._id;
          done();
          });

    })
  })
  describe('GET', function(){
    it('function should get all state information of a country with parameter countryID from db', function(done){
    chai.request(app)
        .get('/api/1.0/state/'+countryinfo['countryID'])
        .end(function (err,res) {
          for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('s_name');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
  describe('GET', function(){
    it('function should get specific state information with parameter stateID from db', function(done){
    chai.request(app)
        .get('/api/1.0/sp_state/'+stateinfo['stateID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
   describe('GET', function(){
    it('function should get all state information  from db', function(done){
    chai.request(app)
        .get('/api/1.0/state/')
        .end(function (err,res) {
          for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('s_name');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
   describe('PUT', function(){
    it('function should update state Information', function(done){
      var updatestateinformation={s_name:'sample state update',s_code:"1234"};
    chai.request(app)
        .put('/api/1.0/sp_state/'+stateinfo['stateID'])
        .send(updatestateinformation) 
        .end(function (err,res) {
          expect(res.body.s_name).to.equal('sample state update');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
   //location data
    describe('POST', function(){
    it('function should post location information to db parameter  countryID and stateID', function(done){
      var locationinformation={location_name:'sample location'};
    chai.request(app)
        .post('/api/1.0/location/'+countryinfo['countryID']+'/'+stateinfo['stateID'])
        .send(locationinformation)
        .end(function (err,res) {
          expect(res.body.location_name).to.equal('sample location');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          locationinfo['locationID']=res.body._id;
          done();
          });

    })
  })
    describe('GET', function(){
    it('function should get all location information of a state with parameter stateID from db', function(done){
    chai.request(app)
        .get('/api/1.0/location/'+stateinfo['stateID'])
        .end(function (err,res) {
           for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('location_name');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
    describe('GET', function(){
    it('function should get specific location information with parameter locationID from db', function(done){
    chai.request(app)
        .get('/api/1.0/specificlocation/'+locationinfo['locationID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
     describe('GET', function(){
    it('function should get all location information  from db', function(done){
    chai.request(app)
        .get('/api/1.0/location/')
        .end(function (err,res) {
           for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('location_name');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
     describe('PUT', function(){
    it('function should update location Information', function(done){
      var updatelocationinformation={location_name:'sample location update'};
    chai.request(app)
        .put('/api/1.0/specificlocation/'+locationinfo['locationID'])
        .send(updatelocationinformation) 
        .end(function (err,res) {
          expect(res.body.location_name).to.equal('sample location update');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
     describe('GET', function(){
    it('function should get all address information  from db includes (location,state and country)', function(done){
    chai.request(app)
        .get('/api/1.0/address')
        .end(function (err,res) {
          for(var i=0;i<res.body.length;i++){
          expect(res.body[i]).to.have.property('location_name');
        }
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })

 describe('Delete', function(){
    it('function should delete specific location from db parameter locationID', function(done){
    chai.request(app)
        .delete('/api/1.0/specificlocation/'+locationinfo['locationID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })

   describe('Delete', function(){
    it('function should delete specific state from db parameter stateID', function(done){
    chai.request(app)
        .delete('/api/1.0/sp_state/'+stateinfo['stateID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })

  describe('Delete', function(){
    it('function should delete specific country from db parameter countryID', function(done){
      
    chai.request(app)
        .delete('/api/1.0/country/'+countryinfo['countryID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
   
})

