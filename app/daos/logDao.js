'use strict';

var async = require('async');
var mongoose = require('mongoose');
var UserProfile = mongoose.model('User_Profile');
var Logs = mongoose.model('logs');


// Save data
exports.savelogs = function savelogs(id,data, next) {
      async.waterfall([
            function(callback){
                UserProfile.findOne({userID: id},function(err, result){
                    if(err){ throw err;}
                    callback(null,result);
                })
            },
        	function(result,callback){
                if(result === null){
                    callback({return:true},null);
                }else{
                    data['userprofileID'] = result._id;
                    data['userID'] = result.userID;
                    console.log('ready to save:',data);
                    Logs.create(data,function(err, result){
                        if(err){ throw err;}
                        callback(null, result);
                    });
                }
        	}
        ],next)
};

exports.getalllogs=function getalllogs(next){
    Logs
      .find(next)
      .populate('userprofileID','u_lname u_fname u_image')
      .exec(function(err, data){
        if(err) return handleError(err);
      });
};

exports.findlogs = function findlogs(id, next) {
   Logs
   		.find({userID: id},next)
      .populate('userprofileID','u_lname u_fname u_image')
   		.exec(function(err, data){
   			if(err) return handleError(err);
   		});
};

exports.deleteLogsByUserId = function deleteLogsByUserId(id, next){
    Logs.findByIdAndRemove(id,next);
};
