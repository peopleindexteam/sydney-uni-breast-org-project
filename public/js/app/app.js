//inject angular file upload directives and services.
var nodeMongo = nodeMongo || {};

nodeMongo.Controllers = angular.module('nodeMongo.controllers', []);
nodeMongo.Services = angular.module('nodeMongo.services', []);

var config = function($routeProvider, $httpProvider, $locationProvider) {

    var checkLoggedin = function($q, $timeout, $http, $location, $rootScope) {
        // Initialize a new promise
        var deferred = $q.defer();

        // Make an AJAX call to check if the user is logged in
        $http.get('/api/1.0/loggedin').success(function(user) {
            // Authenticated
            if (user !== '0')
                $timeout(deferred.resolve, 0);
            // Not Authenticated
            else {
                $rootScope.message = 'You need to log in.';
                $timeout(function() {
                    deferred.reject();
                }, 0);
                $location.url('/');
            }
        });
        return deferred.promise;
    };

    $httpProvider.interceptors.push(function($q, $location) {
        return function(promise) {
            return promise.then(
                // Success: just return the response
                function(response) {
                    return response;
                },
                // Error: check the error status to get only the 401
                function(response) {
                    if (response.status === 401)
                        $location.url('/');
                    return $q.reject(response);
                }
            );
        }
    });

    $routeProvider
        .when('/', {
            templateUrl: '/public/views/dashboard.html'
        })
        .when('/login', {
            templateUrl: '/public/views/login.html',
        })
        .when('/request', {
            templateUrl: '/public/views/requesting.html',
            controller:'RequestCtrl',
        })
        .when('/roles', {
            templateUrl: '/public/views/roles.html',
            controller:'RoleCtrl',
        })
        .when('/address', {
            templateUrl: '/public/views/address.html',
            controller:'AddressCtrl',
        })
        .when('/logs', {
            templateUrl: '/public/views/logs.html',
            controller:'LogCtrl',
        })
        .when('/user', {
            templateUrl: '/public/views/userRegistration.html',
            controller:'UserCtrl',
        })
        .when('/profile', {
            templateUrl: '/public/views/profile.html',
        })
        .when('/register', {
            templateUrl: '/public/views/registration.html',
        })
        .otherwise({
            redirectTo: '/'
        });
}

angular
    .module('nodeMongo', ['angularFileUpload', 'ngRoute', 'nodeMongo.controllers', 'nodeMongo.services'])
    .config(config)
    .run(function($rootScope, $http) {
        $rootScope.message = '';

        // Logout function is available in any pages
        $rootScope.logout = function() {
            $rootScope.message = 'Logged out.';
            $http.post('/api/logout');
            window.location.reload();
        };
    });
