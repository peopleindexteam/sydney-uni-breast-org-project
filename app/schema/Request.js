'use strict';

module.exports = function(mongoose){
	var RequestSchema = new mongoose.Schema({
		date_request:{
            type: Date,
            default: Date.now
        },
        purpose:String,
        approve_date:Date,
        approve_tag:String,
        valid_until:Date,
        filters:String,         // request details
                     // Documents
        test_sets:[{
            file_name:String,
            file_type:String,
            url:String,
            file_size:Number
        }],
        images:[{
            file_name:String,
            file_type:String,
            url:String,
            file_size:Number
        }],
        reading:[{
            file_name:String,
            file_type:String,
            url:String,
            file_size:Number
        }],
        results:[{
            file_name:String,
            file_type:String,
            url:String,
            file_size:Number
        }],
        userID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User_Account'
        },
        userprofileID:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User_Profile'
        },
        countryID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'country'
        },
        stateID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'state'
        },
        locationID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'location'
        }
	});
	mongoose.model('Request', RequestSchema);
};
