'use strict';


var userLoginValidate = require('../validation/userLogin'),
    userValidate = require('../validation/user'),
    countryValidate = require('../validation/address'),
    stateValidate = require('../validation/state'),
    locationValidate = require('../validation/location'),
    roleValidate = require('../validation/role'),
    fs = require('fs-extra'),
    requestValidate = require('../validation/request');

var authctrl = require('../controllers/authenticationcontroller'),
    userctrl = require('../controllers/usercontroller.js'),
    addressctrl = require('../controllers/addresscontroller.js'),
    logctrl = require('../controllers/logcontroller.js'),
    rolectrl = require('../controllers/rolecontroller.js'),
    reqctrl = require('../controllers/requestcontroller.js');


module.exports = function(app, passport, isLoggedIn) {

    app.route('/').get(function(req, res) {
        res.render('index');
    });


    //--------------------------------Authentication-----------------------------//

    app.route('/api/1.0/login')
        .get(authctrl.viewLogin)
        .post(userLoginValidate.validateLogin,passport.authenticate('user'),function(req, res){
           
        console.log(req.body);
       res.send("Successfully login");
        });

    app.route('/api/1.0/logout')
        .get(authctrl.logout);

    //---------------------------------user routes-------------------------------//

    app.route('/api/1.0/user')
        .post(userValidate.validateUser, userctrl.saveUser)
        .get(userctrl.getUser);


    //---Get_specificUser,deleteSpecificUser-----------//
    app.route('/api/1.0/user/:id')
        .get(userctrl.getUserById)
        .put(userValidate.validateUser, userctrl.updateUserById)
        .delete(userctrl.deleteUserById);

    //---------------------------Upload files-----------------------------//

    app.route('/api/1.0/user/uploadImage/:_id')
        .put(userctrl.uploadImage);


    //---------save-userrequest-requestdetails-documents--------//

    app.route('/api/1.0/request/:_id')
        .post(requestValidate.requestValidate, reqctrl.saveRequestByUserId)

    app.route('/api/1.0/requestSP/:id')
        .get(reqctrl.getRequestById)
        .put(reqctrl.saveRequestById)
        .delete(reqctrl.deleteRequestById)

    //find request of specific user with the parameter  userID and approval stat if YES or NO
    app.route('/api/1.0/request/:id/:apstat')
        .get(reqctrl.getRequestStatusByUserId)

    app.route('/api/1.0/request_status/:apstat')
        .get(reqctrl.getRequestStatus)

    //delete specific upload file in array {request} parameter requestID, objectId and specific reqfileID
    app.route('/api/1.0/request/deleteupload/:reqID/:obID/:reqfile')
        .delete(reqctrl.deleteRequestUpload)

    app.route('/api/1.0/request/upload/:_id')
        .put(reqctrl.updateRequestById)

    //add more objects in an array (request testsets,images,result,reading)
    app.route('/api/1.0/request/uploadmore/:_id')
        .put(reqctrl.uploadMoreRequest)

    //-----------------------------------country_routes----------------------------------//

    //---Save_country,Getallcountries,updatecountry-----------//
    app.route('/api/1.0/country')
        .post(countryValidate.validateCountry, addressctrl.saveCountry)
        .get(addressctrl.getCountry)

    app.route('/api/1.0/country/:id')
        .put(countryValidate.validateCountry, addressctrl.updateCountryById)
        .get(addressctrl.getCountryById)
        .delete(addressctrl.delCountryByID)

    //get all state in state collection
    app.route('/api/1.0/state')
        .get(addressctrl.getState)

    //save  state to a country with the parameter of countryID
    app.route('/api/1.0/state/:id')
        .post(stateValidate.validateState, addressctrl.saveStateByCountryId)
        .get(addressctrl.getStateByCountryId)

    //get state details based on its own id
    app.route('/api/1.0/sp_state/:id')
        .get(addressctrl.getStateById)
        .put(stateValidate.validateState, addressctrl.updateStateById)
        .delete(addressctrl.deleteStateByID)

    //save location to state with the parameter of countryID and stateID
    app.route('/api/1.0/location/:idc/:ids')
        .post(locationValidate.validateLocation, addressctrl.saveLocationByCountryIdStateId)

    //get all location in collections
    app.route('/api/1.0/location/')
        .get(addressctrl.getLocation)

    //get all location of a state with the parameter of stateID
    app.route('/api/1.0/location/:id')
        .get(addressctrl.getLocationById)


    //get specific location with the parameter of locationID
    app.route('/api/1.0/specificlocation/:id')
        .get(addressctrl.getSpecificLocationById)
        .put(locationValidate.validateLocation, addressctrl.updateSpecificLocationById)
        .delete(addressctrl.deleteSpecificLocationByID)

    //get all address that includes the location , state and country
    app.route('/api/1.0/address')
        .get(addressctrl.getAddress)

    //-----------------------------------page_routes----------------------------------//

    //---Save_logs,Getalllogs,updatelogs-----------//
    app.route('/api/1.0/log/')
        .get(logctrl.getLogs)

    //---getSpecificlogsbytheuser-----------//
    app.route('/api/1.0/log/:id')
        .get(logctrl.getLogsByUserId)
        .post(logctrl.saveLogsByUserId)
        .delete(logctrl.deleteLogsByUserId)

    app.route('/api/1.0/roles/')
        .post(roleValidate.validateRoles, rolectrl.saveRoles)
        .get(rolectrl.getRoles)

    app.route('/api/1.0/roles/:id')
        .get(rolectrl.getRolesById)
        .put(roleValidate.validateRoles, rolectrl.updateRolesById)
        .delete(rolectrl.deleteRolesById)

    app.route('/api')
        .get(function onRequest(req, res, next) {
            var file = 'public/dist/index.html';
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                res.send(data);
            });
        });

    app.route('/api/BreastCancerAPI')
        .get(function onRequest(req, res, next) {
            var file = 'swagger/swagger.json';
            fs.readFile(file, 'utf8', function(err, data) {
                if (err) {
                    console.log('Error: ' + err);
                    return;
                }
                data = JSON.parse(data);
                res.send(data);
            });
        });
};
