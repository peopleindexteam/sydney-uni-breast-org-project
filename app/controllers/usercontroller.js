'use strict';

var path = require('path'),
    fs = require('fs-extra'),
    async = require('async');

var userService = require('../services/userService').User;
var user = new userService();

var cb = require('./../utils/callback');


exports.saveUser = function onRequest(req, res) {
    user.saveuser(req.body, cb.setupResponseCallback(res));
}


exports.getUser = function onRequest(req, res, next) {
    user.getalluser(cb.setupResponseCallback(res));
}


exports.getUserById = function onRequest(req, res, next) {
    user.findUser(req.params.id, cb.setupResponseCallback(res));
}


exports.updateUserById = function onRequest(req, res, next) {
    console.log(req.body);
    user.updateuser(req.params.id, req.body, cb.setupResponseCallback(res));
}


exports.deleteUserById = function onRequest(req, res, next) {
    user.deleteUser(req.params.id, cb.setupResponseCallback(res));
}


exports.uploadImage = function onRequest(req, res, next) {
    console.log('mao ni gikan sa index',req.files);
    var u_image = new Object(); // declare u_image object
    var data = new Object(); // declare data object to push from u_image object

    var targetPath = path.resolve('./public/uploads/img/users/' + req.params._id + '-' + req.files.file.name); // locate the target Path to save the image.
    data['file_name'] = req.files.file.name;
    data['file_type'] = req.files.file.mimetype;
    data['url'] = './public/uploads/img/users/' + req.params._id + '-' + req.files.file.name;
    data['file_size'] = req.files.file.size;
    u_image['u_image'] = data;

    console.log(targetPath);
    console.log(u_image);
    if (path.extname(req.files.file.originalname).toLowerCase() === '.png' || path.extname(req.files.file.originalname).toLowerCase() === '.jpg' || path.extname(req.files.file.originalname).toLowerCase() === '.jpeg' || path.extname(req.files.file.originalname).toLowerCase() === '.gif') {
        fs.copy(req.files.file.path, targetPath, function(err) {
            if (err) throw err;
            user.updateuserimage(req.params._id, u_image, cb.setupResponseCallback(res));
            console.log("Successfully Updated");
        });
    }
}
