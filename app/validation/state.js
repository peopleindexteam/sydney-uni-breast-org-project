'use strict';

exports.validateState = function(req, res, next) {

    req.checkBody('s_name', 'Please enter a State Name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};