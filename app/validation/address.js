'use strict';

exports.validateCountry = function(req, res, next) {

    req.checkBody('c_name', 'Please enter a Country Name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};
