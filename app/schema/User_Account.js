'use strict';

module.exports = function(mongoose){
	var User_AccountSchema = new mongoose.Schema({
		u_email:String,
        u_password:String,
        u_isverified:Number,
	});

	mongoose.model('User_Account', User_AccountSchema);
};
