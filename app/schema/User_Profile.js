'use strict';

module.exports = function(mongoose){
	var User_ProfileSchema = new mongoose.Schema({
		u_lname:String,
        u_fname:String,
        organization:String,
        u_image:{
            file_name:String,
            file_type:String,
            url:String,
            file_size:Number
        },
		countryID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'country'
        },
        stateID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'state'
        },
        locationID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'location'
        },
        userID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User_Account'
        },
        roleID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Roles'
        }
	});
	mongoose.model('User_Profile', User_ProfileSchema);
};
