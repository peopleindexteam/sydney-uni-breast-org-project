'use strict';

var env = process.env.NODE_ENV || 'development',
    application = require('./config/application'),
    express = require('express'),
    bunyan = require('bunyan'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    config = require('./config/environment/' + env),
    middleware = require('./app/utils/middleware'),
    Database = require('./app/utils/database').Database,
    database = new Database(mongoose, config),
    log = bunyan.createLogger({
        name: config.app_name
    }),
    app = express();



require(application.utils + 'helper')(database, app, log);
require(application.utils + 'loadschema')(mongoose);
require(application.config + 'express')(app, passport);
require(application.config + 'passport')(passport,mongoose,middleware.isLoggedIn);
require(application.routes + 'email')(app);
require(application.routes)(app,passport,middleware.isLoggedIn);

module.exports = app;
