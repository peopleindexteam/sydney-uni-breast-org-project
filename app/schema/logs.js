'use strict';

module.exports = function(mongoose){
	var logsSchema = new mongoose.Schema({
		DateTran: {
            type: Date,
            default: Date.now
        },
        logTranType:[{
        	desc: String
        }],
        page:[{
        	name: String
        }],
        userprofileID:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User_Profile'
        },
		userID: {
			type: mongoose.Schema.Types.ObjectId,
			ref: 'User_Account'
		}
	});
	
	mongoose.model('logs', logsSchema);
}
