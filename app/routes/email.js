'use strict';

var emailCtrl = require('../services/emailServices');
var cb = require('./../utils/callback');

module.exports = function(app) {


    app.route('/api/1.0/email/recover')
        .post(function onRequest(req, res, next) {
            emailCtrl.recoverPassword(req.body, cb.setupResponseCallback(res));
        });

    // app.route('/api/1.0/email/contact').post(emailCtrl.sendMail);

    // app.route('/api/1.0/email/subscription').post(emailCtrl.sendMail);

    // app.route('/api/1.0/email/registration').post(emailCtrl.sendMail);

}
