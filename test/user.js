

var chai= require('chai');
var chaiHttp= require('chai-http');
var	app='http://localhost:3000';
var expect = chai.expect;
var fs= require('fs');

	chai.use(chaiHttp);
	chai.should();

describe('BreastCancers Api testing (USER)', function(){
	var userinfo={};

 describe('POST', function(){
    it('function should post user information to db', function(done){
    	var userinformation={u_lname:'examplelname',u_fname:'examplefname',organization:'forloop',u_email:'example@unittest.com',u_password:'123456789'};
		chai.request(app)
			  .post('/api/1.0/user')
			  .send(userinformation)
			  .end(function (err,res) {
			  	expect(res.body.u_lname).to.equal('examplelname');
			  	expect(res.body.u_fname).to.equal('examplefname');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
     			userinfo['userID']=res.body.userID;
     			
    			done();
  				});

    })
  })
   describe('GET', function(){
    it('function should get userinformation to db', function(done){
    	
		chai.request(app)
			  .get('/api/1.0/user/'+userinfo['userID'])
			  .end(function (err,res) {
			  	expect(res.body.u_lname).to.equal('examplelname');
			  	expect(res.body.u_fname).to.equal('examplefname');
			  	expect(res.body.organization).to.equal('forloop');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
    			done();
  				});

    })
  })
   describe('PUT', function(){
    it('function should update User Information', function(done){
    	var updateuserinformation={u_lname:'doe',u_fname:'john',organization:'forloop',u_email:'example@unittest.com',u_password:'123456789'};
		chai.request(app)
			  .put('/api/1.0/user/'+userinfo['userID'])
			  .send(updateuserinformation)
			  .end(function (err,res) {
			  expect(res.body.u_lname).to.equal('doe');
			  	expect(res.body.u_fname).to.equal('john');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
    			done();

  				});


    })
  })
   describe('PUT', function(){
    it('function should upload image for the user', function(done){
		chai.request(app)
			  .put('/api/1.0/user/uploadImage/'+userinfo['userID'])
			  .attach('file', '../test/userimage/test.jpg')
			  .end(function (err,res) {
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
     			describe('CHECK file ',function(){
     				it('function should check if uploaded succesfully save to directory',function(done){
     					expect(fs.existsSync('.'+res.body.u_image.url)).to.be.true;
     					done();
     				})
     			})
    			done();

  				});

    })
  })
   describe('POST', function(){
    it('function should have accounts to db to login', function(done){
    	
		chai.request(app)
			  .post('/api/1.0/login')
			  .send({ username: 'example@unittest.com', password: '123456789' })
			  .end(function (err,res) {
			  	expect(res.text).to.equal('Successfully login');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
    			done();

  				});

    })
  })
    describe('Delete', function(){
    it('function should delete accounts from db', function(done){
    	
		chai.request(app)
			  .delete('/api/1.0/user/'+userinfo['userID'])
			  .end(function (err,res) {
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
    			done();

  				});

    })
  })
})

