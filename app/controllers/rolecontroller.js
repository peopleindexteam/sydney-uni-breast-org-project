'use strict';

var roleService = require('../services/roleService').Role;
var roles = new roleService();

var cb = require('./../utils/callback');


exports.saveRoles = function onRequest(req, res, next) {
    roles.saverole(req.body, cb.setupResponseCallback(res));
}

exports.getRoles = function onRequest(req, res, next) {
    roles.getallrole(cb.setupResponseCallback(res));
}

exports.getRolesById = function onRequest(req, res, next) {
    roles.findrole(req.params.id, cb.setupResponseCallback(res));
}

exports.updateRolesById = function onRequest(req, res, next) {
    roles.updaterole(req.params.id, req.body, cb.setupResponseCallback(res));
}

exports.deleteRolesById = function onRequest(req, res, next) {
    roles.deleteRolesById(req.params.id, cb.setupResponseCallback(res));
}
