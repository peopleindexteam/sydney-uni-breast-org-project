'use strict';


var requestDao = require('../daos/requestDao');

 function Request() {
    this.requestDao = requestDao;
 }

 Request.prototype.saverequest = function(_id,data, next) {
    requestDao.saverequest(_id,data, next);
 };

Request.prototype.findrequestdetails = function(id,appstat,next){
    requestDao.findrequestdetails(id,appstat,next);
};
Request.prototype.deletespecificupload=function(reqID,obID,reqfile,next){
	requestDao.deletespecificupload(reqID,obID,reqfile,next);
}
Request.prototype.getallrequest= function(appstat,next){
	requestDao.getallrequest(appstat,next);
};
Request.prototype.updaterequest= function(id,data,next){
	requestDao.updaterequest(id,data,next);
};
Request.prototype.updaterequestmore= function(id,data,next){
	requestDao.updaterequestmore(id,data,next);
};
Request.prototype.getsprequest= function(id,next){
	requestDao.getsprequest(id,next);
}
Request.prototype.deleteRequestById = function(id,next){
	requestDao.deleteRequestById(id,next);
}

exports.Request = Request;
