'use strict';

var async = require('async');
var mongoose = require('mongoose');
var User_Account = mongoose.model('User_Account');
var User_Profile = mongoose.model('User_Profile');
var emailCtrl = require('../services/emailServices');
var bcrypt = require('bcrypt-nodejs');

// Save data
exports.saveuser = function saveuser(data, next) {
    async.waterfall([
        function(callback){
            User_Account.findOne({u_email: data.u_email}, function(err, data){
                if(err){ throw err;}
                if(data){
                    callback({return:true,msg:'Email/Username already exist'},null);
                }else{
                    callback(null,data);
                }
            });
        },
        function(result,callback){ // Save data to User_Account
            data['u_isverified'] = 0;
            data['u_password'] = bcrypt.hashSync(data.u_password, bcrypt.genSaltSync(8), null);
    		User_Account.create(data,function(err, result){
                if(err){ throw err;}
			     callback(null, result);
                 emailCtrl.emailVerification(data.u_email,function(err,data){
                    if(err){ throw err;}
                    console.log('Email Successfully send');
                });
            });
    	},
    	function(result,callback){ // Save data to User_Profile
    		data['userID'] = result._id;
    		User_Profile.create(data,function(err, result){
                if(err){ throw err;}
    			callback(null,result);
    		});
        }
    ],next)
};

exports.updateuser = function updatuser(id,data,next) {

    console.log('data from dao:', data);
    console.log('data from dao:', id);
    async.waterfall([
        function(callback){ // find the userID then update for User Account
            data['u_password'] = bcrypt.hashSync(data.u_password, bcrypt.genSaltSync(8), null);
            User_Account.findByIdAndUpdate(id, data, function(err, result){
                if(err){ throw err;}
                callback(null,result);
            });
        },
        function(result, callback){ // find the userID then update for User Profile
            User_Profile.findOneAndUpdate({userID: id},data,function(err, result){
                if(err){ throw err;}
                callback(null, result);
            }); 
        }
    ], next)
}
exports.updateuserimage = function updateuserimage(id,data,next) {
    async.waterfall([
        function(callback){ // find the userID then update for User Account
            User_Account.findByIdAndUpdate(id, data, function(err, result){
                if(err){ throw err;}
                callback(null,result);
            });
        },
        function(result, callback){ // find the userID then update for User Profile
            User_Profile.findOneAndUpdate({userID: id},data,function(err, result){
                if(err){ throw err;}
                callback(null, result);
            }); 
        }
    ], next)
}

exports.getalluser=function getalluser(next){
    var query = User_Profile.find();
    query
        .populate('userID','u_email u_isverified roleID')
        .populate('roleID')
        .populate('countryID')
        .populate('locationID')
        .populate('stateID')
        .exec(next);
};

exports.findUser = function findUser(id, next) {
    var query = User_Profile.findOne({userID: id});
    query
        .populate('userID','u_email u_isverified')
        .populate('roleID')
        .populate('countryID')
        .populate('locationID')
        .populate('stateID')
        .exec(next);
};

//remove data
exports.deleteUser = function deleteUser(id, next) {
    async.waterfall([
        function(callback){
            User_Profile.findOneAndRemove({userID: id}, function(err, result){
                if(err){ throw err;}
                callback(null,result);
            });
        },
        function(result,callback){
            User_Account.findByIdAndRemove(id, function(err, result){
                if(err){ throw err;}
                callback(null,result);
            });
        }
    ], next);
}
