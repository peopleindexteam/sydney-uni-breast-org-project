'use strict';

module.exports = {
    dbUrl: process.env.DB_URL || 'mongodb://localhost:27017/BreastCancer',
    db_user: process.env.DB_USER || 'BreastCancer',
    db_password: process.env.DB_USER || '12345',
    port: process.env.APP_PORT || 3000,
    socket_port: process.env.SOCKET_PORT || 3333,
    app_name: process.env.APP_NAME || "BreastCancer",
    api_host_url: process.env.API_HOST_URL || 'http://localhost:3000',
    api_version: '/api/v1',
    mailgun_public_key: 'pubkey-eba806ce4f01ea08b5e79ca977f523bf',
    mailgun_api_key: 'key-9fc41d653fa0eed2e7b3ee5b20f1fe6a',
    mailgun_domain: 'sandboxfcbb916b2714462fafb4848ad489aedc.mailgun.org'
};
