'use strict';


var userDao = require('../daos/userDao');

 function User() {
    this.userDao = userDao;
 }

 User.prototype.saveuser = function(data, next) {
    userDao.saveuser(data, next);
 };
 User.prototype.getalluser= function(next){
    userDao.getalluser(next);
 };
 User.prototype.findUser = function findUser(id,next){
     userDao.findUser(id,next);
 };
 User.prototype.deleteUser= function deleteUser(id, next){
	userDao.deleteUser(id, next);
 };
 User.prototype.updateuser = function(id,data,next) {
 	userDao.updateuser(id,data,next)
 };
  User.prototype.updateuserimage = function(id,data,next) {
 	userDao.updateuserimage(id,data,next)
 };

exports.User = User;
