'use strict';

exports.validateLocation = function(req, res, next) {

    req.checkBody('location_name', 'Please enter a Location Name').notEmpty();

    var errors = req.validationErrors();
    if (errors) {
        res.status(200).send(errors);
    } else {
        next();
    }
};
