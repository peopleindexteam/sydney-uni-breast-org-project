

var chai= require('chai');
var chaiHttp= require('chai-http');
var	app='http://localhost:3000';
var fs=require('fs');
var expect = chai.expect;

	chai.use(chaiHttp);
	chai.should();

describe('BreastCancers Api testing For (REQUEST)', function(){
	var requestinfo={};
  var userinfo={};

  describe('POST', function(){
    it('function should post user information to db to use the userID for Request sample post', function(done){
      var userinformation={u_lname:'examplelname',u_fname:'examplefname',organization:'forloop',u_email:'example@example.com',u_password:'123456789'};
    chai.request(app)
        .post('/api/1.0/user')
        .send(userinformation)
        .end(function (err,res) {
          expect(res.body.u_lname).to.equal('examplelname');
          expect(res.body.u_fname).to.equal('examplefname');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          userinfo['userID']=res.body.userID;
          done();
          });

    })
  })
 

 describe('POST', function(){
    it('function should post request information to db', function(done){
    	var requestinformation={purpose:'sample request',filters:"None",valid_until:"1-25-17"};
		chai.request(app)
			  .post('/api/1.0/request/'+userinfo['userID'])
        .send(requestinformation)
			  .end(function (err,res) {
			  	expect(res.body.purpose).to.equal('sample request');
			  	expect(err).to.be.null;
     			expect(res).to.have.status(200);
          requestinfo['requestID']=res.body._id;
    			done();
  				});

    })
  })
  describe('GET', function(){
    it('function should get all request with approve value is NO parameter approve_tag', function(done){
    chai.request(app)
        .get('/api/1.0/request_status/NO')
        .end(function (err,res) {
          expect(res.body[0].approve_tag).to.equal('NO');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
   describe('GET', function(){
    it('function should get all request with the specific user parameter userID and approve status "NO"', function(done){
    chai.request(app)
        .get('/api/1.0/request/'+userinfo['userID']+'/NO')
        .end(function (err,res) {
          expect(res.body[0].approve_tag).to.equal('NO');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
  describe('PUT', function(){
    it('function should upload files for the user request parameter requestID', function(done){
    chai.request(app)
        .put('/api/1.0/request/upload/'+requestinfo['requestID'])
        .attach('test_sets', '../test/userimage/test.jpg')
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          requestinfo['objectID']=res.body.test_sets[0]._id;
          describe('CHECK file ',function(){
            it('function should check if uploaded succesfully save to directory',function(done){
              expect(fs.existsSync('.'+res.body.test_sets[0].url)).to.be.true;
              done();
            })
          })
          done();
          });

    })
  })
  describe('PUT', function(){
    it('function should upload more files for the user request parameter requestID', function(done){
    chai.request(app)
        .put('/api/1.0/request/uploadmore/'+requestinfo['requestID'])
        .attach('test_sets', '../test/requestfile/Mindanao Jobs Website.docx')
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          describe('CHECK file ',function(){
            it('function should check if more uploads uploaded succesfully, save to directory',function(done){
              for(var i=0;i<res.body.test_sets.length;i++){
              expect(fs.existsSync('.'+res.body.test_sets[0].url)).to.be.true;
            }
              done();
            })
          })
          done();
          });

    })
  })
  describe('PUT', function(){
    it('function should approve request or set approve_tag value equal(YES) parameter requestID', function(done){
      var approverequest={approve_tag:'YES',Read:0,Write:0,Delete:0};
    chai.request(app)
        .put('/api/1.0/requestSP/'+requestinfo['requestID'])
        .send(approverequest)
        .end(function (err,res) {
          expect(res.body.approve_tag).to.equal('YES');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
  describe('GET', function(){
    it('function should get all request with approve value is YES parameter approve_tag', function(done){
    chai.request(app)
        .get('/api/1.0/request_status/YES')
        .end(function (err,res) {
          expect(res.body[0].approve_tag).to.equal('YES');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
describe('GET', function(){
    it('function should get all request with the specific user parameter userID and approve status "YES"', function(done){
    chai.request(app)
        .get('/api/1.0/request/'+userinfo['userID']+'/YES')
        .end(function (err,res) {
          expect(res.body[0].approve_tag).to.equal('YES');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
describe('GET', function(){
    it('function should get specific request with the parameter requestID', function(done){
    chai.request(app)
        .get('/api/1.0/requestSP/'+requestinfo['requestID'])
        .end(function (err,res) {
          expect(res.body[0]).to.have.property('purpose');
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();
          });

    })
  })
describe('Delete', function(){
    it('function should specific file in the specific request parameters (requestID, fileID,categories),test for category test_sets', function(done){
      
    chai.request(app)
        .delete('/api/1.0/request/deleteupload/'+requestinfo['requestID']+'/'+requestinfo['objectID']+'/test_sets')
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
 describe('Delete', function(){
    it('function should delete user info used from testing', function(done){
      
    chai.request(app)
        .delete('/api/1.0/user/'+userinfo['userID'])
        .end(function (err,res) {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          done();

          });

    })
  })
   
})

