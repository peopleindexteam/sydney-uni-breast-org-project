'use strict';

module.exports = function(mongoose){
	var countrySchema = new mongoose.Schema({
		c_name: String,
		c_Desc: String
	});

	mongoose.model('country', countrySchema);
}