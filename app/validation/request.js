'use strict';

exports.requestValidate = function(req, res, next){
	req.checkBody('purpose', 'Please enter a Purpose').notEmpty();

	var error = req.validationErrors();
	if (error) {
       	res.status(200).send(error);
    } else {
        next();
    }
};