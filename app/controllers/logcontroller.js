'use strict';

var logService = require('../services/logService').Log;
var log = new logService();

var cb = require('./../utils/callback');


exports.getLogs = function onRequest(req, res, next) {
    log.getalllogs(cb.setupResponseCallback(res));
}

exports.getLogsByUserId = function onRequest(req, res, next) {
    log.findlogs(req.params.id, cb.setupResponseCallback(res));
}

exports.saveLogsByUserId = function onRequest(req, res, next) {
    log.savelogs(req.params.id, req.body, cb.setupResponseCallback(res));
}

exports.deleteLogsByUserId = function onRequest(req, res, next) {
    log.deleteLogsByUserId(req.params.id, cb.setupResponseCallback(res));
}
