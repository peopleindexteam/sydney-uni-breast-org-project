'use strict';


var path = require('path'),
    fs = require('fs-extra'),
    async = require('async');


var requestService = require('../services/requestService').Request;
var request = new requestService();

var cb = require('./../utils/callback');


exports.saveRequestByUserId = function onRequest(req, res, next) {
    console.log('data in body:',req.body);

    var d_file_object = {};
    d_file_object = req.body;
    var arrayALL =[];

    var test_sets_dataArray =[];
    var images_dataArray =[];
    var reading_dataArray =[];
    var results_dataArray =[];

    // upload test sets
    async.waterfall([
        function(callback) {
            if (req.files.test_sets) {

                var test_sets_Object = {};
                var test_sets_data = {};
                var tesetsarrayALLtesets =[];
                var sd_file_object = {};
                var testsetsPATH = path.resolve('./public/uploads/request/testsets/' + Math.floor(Date.now() / 1000) + '-' + req.files.test_sets.name);
                test_sets_data['file_name'] = req.files.test_sets.name;
                test_sets_data['file_type'] = req.files.test_sets.mimetype
                test_sets_data['url'] = './public/uploads/request/testsets/' + Math.floor(Date.now() / 1000) + '-' + req.files.test_sets.name;
                test_sets_data['file_size'] = req.files.test_sets.size;
                test_sets_dataArray.push(test_sets_data);

                if (path.extname(req.files.test_sets.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.doc' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.docx' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.pdf' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.test_sets.path, testsetsPATH, function(err) {
                        if (err) throw err;
                        d_file_object['test_sets'] = test_sets_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {
            if (req.files.images) { // upload images
                var images_Object = {};
                var images_data = {};
                var imagearrayALL =[];
                var imaged_file_object = {};
                var imagesPATH = path.resolve('./public/uploads/request/images/' + Math.floor(Date.now() / 1000) + '-' + req.files.images.name);

                images_data['file_name'] = req.files.images.name;
                images_data['file_type'] = req.files.images.mimetype
                images_data['url'] = './public/uploads/request/images/' + Math.floor(Date.now() / 1000) + '-' + req.files.images.name;
                images_data['file_size'] = req.files.images.size;
                images_dataArray.push(images_data);

                if (path.extname(req.files.images.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.images.originalname).toLowerCase() === '.doc' || path.extname(req.files.images.originalname).toLowerCase() === '.docx' || path.extname(req.files.images.originalname).toLowerCase() === '.pdf' || path.extname(req.files.images.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.images.path, imagesPATH, function(err) {
                        if (err) throw err;
                        d_file_object['images'] = images_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {

            if (req.files.reading) { // upload readings
                var reading_Object = {};
                var reading_data = {};
                var readingarrayALL =[];
                var readingsd_file_object = {};
                var readingPATH = path.resolve('./public/uploads/request/reading/' + Math.floor(Date.now() / 1000) + '-' + req.files.reading.name);

                reading_data['file_name'] = req.files.reading.name;
                reading_data['file_type'] = req.files.reading.mimetype
                reading_data['url'] = './public/uploads/request/reading/' + Math.floor(Date.now() / 1000) + '-' + req.files.reading.name;
                reading_data['file_size'] = req.files.reading.size;
                reading_dataArray.push(reading_data);

                if (path.extname(req.files.reading.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.reading.originalname).toLowerCase() === '.doc' || path.extname(req.files.reading.originalname).toLowerCase() === '.docx' || path.extname(req.files.reading.originalname).toLowerCase() === '.pdf' || path.extname(req.files.reading.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.reading.path, readingPATH, function(err) {
                        if (err) throw err;
                        d_file_object['reading'] = reading_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {

            if (req.files.results) { // upload readings
                var results_Object = {};
                var results_data = {};
                var resultsarrayALL =[];
                var resultsd_file_object = {};
                var resultsPATH = path.resolve('./public/uploads/request/results/' + Math.floor(Date.now() / 1000) + '-' + req.files.results.name);

                results_data['file_name'] = req.files.results.name;
                results_data['file_type'] = req.files.results.mimetype
                results_data['url'] = './public/uploads/request/results/' + Math.floor(Date.now() / 1000) + '-' + req.files.results.name;
                results_data['file_size'] = req.files.results.size;
                results_dataArray.push(results_data);

                if (path.extname(req.files.results.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.results.originalname).toLowerCase() === '.doc' || path.extname(req.files.results.originalname).toLowerCase() === '.docx' || path.extname(req.files.results.originalname).toLowerCase() === '.pdf' || path.extname(req.files.results.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.results.path, resultsPATH, function(err) {
                        if (err) throw err;

                        d_file_object['results'] = results_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {
            console.log('ready to save:', d_file_object);

            request.saverequest(req.params._id, d_file_object, function(err, result) {
                callback(null, result);

            });
        }
    ], cb.setupResponseCallback(res))

}




exports.updateRequestById = function onRequest(req, res, next) {
    var d_file_object = {};
    var arrayALL =[];
    var test_sets_dataArray =[];
    var images_dataArray =[];
    var reading_dataArray =[];
    var results_dataArray =[];

    // upload test sets
    async.waterfall([
        function(callback) {
            if (req.files.test_sets) {

                var test_sets_Object = {};
                var test_sets_data = {};
                var tesetsarrayALLtesets =[];
                var sd_file_object = {};
                var testsetsPATH = path.resolve('./public/uploads/request/testsets/' + Math.floor(Date.now() / 1000) + '-' + req.files.test_sets.name);
                test_sets_data['file_name'] = req.files.test_sets.name;
                test_sets_data['file_type'] = req.files.test_sets.mimetype
                test_sets_data['url'] = './public/uploads/request/testsets/' + Math.floor(Date.now() / 1000) + '-' + req.files.test_sets.name;
                test_sets_data['file_size'] = req.files.test_sets.size;
                test_sets_dataArray.push(test_sets_data);

                if (path.extname(req.files.test_sets.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.doc' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.docx' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.pdf' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.test_sets.path, testsetsPATH, function(err) {
                        if (err) throw err;
                        d_file_object['test_sets'] = test_sets_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {
            if (req.files.images) { // upload images
                var images_Object = {};
                var images_data = {};
                var imagearrayALL =[];
                var imaged_file_object = {};
                var imagesPATH = path.resolve('./public/uploads/request/images/' + Math.floor(Date.now() / 1000) + '-' + req.files.images.name);

                images_data['file_name'] = req.files.images.name;
                images_data['file_type'] = req.files.images.mimetype
                images_data['url'] = './public/uploads/request/images/' + Math.floor(Date.now() / 1000) + '-' + req.files.images.name;
                images_data['file_size'] = req.files.images.size;
                images_dataArray.push(images_data);

                if (path.extname(req.files.images.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.images.originalname).toLowerCase() === '.doc' || path.extname(req.files.images.originalname).toLowerCase() === '.docx' || path.extname(req.files.images.originalname).toLowerCase() === '.pdf' || path.extname(req.files.images.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.images.path, imagesPATH, function(err) {
                        if (err) throw err;

                        d_file_object['images'] = images_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {

            if (req.files.reading) { // upload readings
                console.log('dli reading');
                var reading_Object = {};
                var reading_data = {};
                var readingarrayALL =[];
                var readingsd_file_object = {};
                var readingPATH = path.resolve('./public/uploads/request/reading/' + Math.floor(Date.now() / 1000) + '-' + req.files.reading.name);

                reading_data['file_name'] = req.files.reading.name;
                reading_data['file_type'] = req.files.reading.mimetype
                reading_data['url'] = './public/uploads/request/reading/' + Math.floor(Date.now() / 1000) + '-' + req.files.reading.name;
                reading_data['file_size'] = req.files.reading.size;
                reading_dataArray.push(reading_data);

                if (path.extname(req.files.reading.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.reading.originalname).toLowerCase() === '.doc' || path.extname(req.files.reading.originalname).toLowerCase() === '.docx' || path.extname(req.files.reading.originalname).toLowerCase() === '.pdf' || path.extname(req.files.reading.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.reading.path, readingPATH, function(err) {
                        if (err) throw err;

                        d_file_object['reading'] = reading_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {

            if (req.files.results) { // upload readings

                console.log('dli');
                var results_Object = {};
                var results_data = {};
                var resultsarrayALL =[];
                var resultsd_file_object = {};
                var resultsPATH = path.resolve('./public/uploads/request/results/' + Math.floor(Date.now() / 1000) + '-' + req.files.results.name);

                results_data['file_name'] = req.files.results.name;
                results_data['file_type'] = req.files.results.mimetype
                results_data['url'] = './public/uploads/request/results/' + Math.floor(Date.now() / 1000) + '-' + req.files.results.name;
                results_data['file_size'] = req.files.results.size;
                results_dataArray.push(results_data);

                if (path.extname(req.files.results.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.results.originalname).toLowerCase() === '.doc' || path.extname(req.files.results.originalname).toLowerCase() === '.docx' || path.extname(req.files.results.originalname).toLowerCase() === '.pdf' || path.extname(req.files.results.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.results.path, resultsPATH, function(err) {
                        if (err) throw err;

                        d_file_object['results'] = results_dataArray;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {
            console.log('ready to save:', d_file_object);

            request.updaterequest(req.params._id, d_file_object, function(err, result) {
                callback(null, result);

            });
        }
    ], cb.setupResponseCallback(res))
}

exports.uploadMoreRequest = function onRequest(req, res, next) {
    var d_file_object = {};
    var arrayALL = [];

    var test_sets_dataArray =[];
    var images_dataArray =[];
    var reading_dataArray =[];
    var results_dataArray =[];

    // upload test sets
    async.waterfall([
        function(callback) {
            if (req.files.test_sets) {

                var test_sets_Object = {};
                var test_sets_data = {};
                var tesetsarrayALLtesets =[];
                var sd_file_object = {};
                var testsetsPATH = path.resolve('./public/uploads/request/testsets/' + Math.floor(Date.now() / 1000) + '-' + req.files.test_sets.name);
                test_sets_data['file_name'] = req.files.test_sets.name;
                test_sets_data['file_type'] = req.files.test_sets.mimetype
                test_sets_data['url'] = './public/uploads/request/testsets/' + Math.floor(Date.now() / 1000) + '-' + req.files.test_sets.name;
                test_sets_data['file_size'] = req.files.test_sets.size;
                test_sets_dataArray.push(test_sets_data);

                if (path.extname(req.files.test_sets.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.doc' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.docx' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.pdf' || path.extname(req.files.test_sets.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.test_sets.path, testsetsPATH, function(err) {
                        if (err) throw err;
                        d_file_object['test_sets'] = test_sets_data;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {
            if (req.files.images) { // upload images
                var images_Object = {};
                var images_data = {};
                var imagearrayALL =[];
                var imaged_file_object = {};
                var imagesPATH = path.resolve('./public/uploads/request/images/' + Math.floor(Date.now() / 1000) + '-' + req.files.images.name);

                images_data['file_name'] = req.files.images.name;
                images_data['file_type'] = req.files.images.mimetype
                images_data['url'] = './public/uploads/request/images/' + Math.floor(Date.now() / 1000) + '-' + req.files.images.name;
                images_data['file_size'] = req.files.images.size;
                images_dataArray.push(images_data);

                if (path.extname(req.files.images.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.images.originalname).toLowerCase() === '.doc' || path.extname(req.files.images.originalname).toLowerCase() === '.docx' || path.extname(req.files.images.originalname).toLowerCase() === '.pdf' || path.extname(req.files.images.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.images.path, imagesPATH, function(err) {
                        if (err) throw err;

                        d_file_object['images'] = images_data;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {

            if (req.files.reading) { // upload readings
                console.log('dli reading');
                var reading_Object = {};
                var reading_data = {};
                var readingarrayALL =[];
                var readingsd_file_object = {};
                var readingPATH = path.resolve('./public/uploads/request/reading/' + Math.floor(Date.now() / 1000) + '-' + req.files.reading.name);

                reading_data['file_name'] = req.files.reading.name;
                reading_data['file_type'] = req.files.reading.mimetype
                reading_data['url'] = './public/uploads/request/reading/' + Math.floor(Date.now() / 1000) + '-' + req.files.reading.name;
                reading_data['file_size'] = req.files.reading.size;
                reading_dataArray.push(reading_data);

                if (path.extname(req.files.reading.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.reading.originalname).toLowerCase() === '.doc' || path.extname(req.files.reading.originalname).toLowerCase() === '.docx' || path.extname(req.files.reading.originalname).toLowerCase() === '.pdf' || path.extname(req.files.reading.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.reading.path, readingPATH, function(err) {
                        if (err) throw err;

                        d_file_object['reading'] = reading_data;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {

            if (req.files.results) { // upload readings

                console.log('dli');
                var results_Object = {};
                var results_data = {};
                var resultsarrayALL =[];
                var resultsd_file_object = {};
                var resultsPATH = path.resolve('./public/uploads/request/results/' + Math.floor(Date.now() / 1000) + '-' + req.files.results.name);

                results_data['file_name'] = req.files.results.name;
                results_data['file_type'] = req.files.results.mimetype
                results_data['url'] = './public/uploads/request/results/' + Math.floor(Date.now() / 1000) + '-' + req.files.results.name;
                results_data['file_size'] = req.files.results.size;
                results_dataArray.push(results_data);

                if (path.extname(req.files.results.originalname).toLowerCase() === '.xlsx' || path.extname(req.files.results.originalname).toLowerCase() === '.doc' || path.extname(req.files.results.originalname).toLowerCase() === '.docx' || path.extname(req.files.results.originalname).toLowerCase() === '.pdf' || path.extname(req.files.results.originalname).toLowerCase() === '.jpg') {
                    fs.copy(req.files.results.path, resultsPATH, function(err) {
                        if (err) throw err;

                        d_file_object['results'] = results_data;
                        callback(null, d_file_object);
                    });
                }
            } else {
                callback(null, true);
            }
        },
        function(result, callback) {
            request.updaterequestmore(req.params._id, d_file_object, function(err, result) {
                callback(null, result);
                console.log('result:', result);
                console.log('error:', err);
            });
        }
    ], cb.setupResponseCallback(res))
}

exports.getRequestById = function onRequest(req, res, next) {
    request.getsprequest(req.params.id, cb.setupResponseCallback(res));
}

exports.saveRequestById = function onRequest(req, res, next) {
    request.updaterequest(req.params.id, req.body, cb.setupResponseCallback(res));
}

exports.deleteRequestById = function onRequest(req, res, next) {
    request.deleteRequestById(req.params.id, cb.setupResponseCallback(res));
}

exports.getRequestStatusByUserId = function onRequest(req, res, next) {
    request.findrequestdetails(req.params.id, req.params.apstat, cb.setupResponseCallback(res));
}

exports.getRequestStatus = function onRequest(req, res, next) {
    request.getallrequest(req.params.apstat, cb.setupResponseCallback(res));
}

exports.deleteRequestUpload = function onRequest(req, res, next) {
    request.deletespecificupload(req.params.reqID, req.params.obID, req.params.reqfile, cb.setupResponseCallback(res));
}
