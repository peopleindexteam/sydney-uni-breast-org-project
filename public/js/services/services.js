'use strict';

var UserFactory = function($http){
	var UserFactory = {};

	UserFactory.saveUser = function(data,cb){
		$http.post('/api/1.0/user',data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};
	
	UserFactory.updateUser=function(id,data,cb){

		$http.put('/api/1.0/user/'+id,data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	UserFactory.getAllUser = function(cb){
		$http.get('/api/1.0/user').success(function(data){
			cb(data);
			console.log(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	UserFactory.UserLogin = function(data,cb){
		$http.post('/login',data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	UserFactory.CurrentUser = function(cb){
		$http.get('/api/1.0/home').success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	UserFactory.getSpUser = function(id,cb){
		$http.get('/api/1.0/user/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	UserFactory.deleteUser = function(id,cb){
		$http.delete('/api/1.0/user/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	UserFactory.saveDocuments = function(data,cb){
		$http.post('/api/1.0/request/54f67d57389d3af81e060aa3',data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	}

	return UserFactory;
}

var RoleFactory = function($http){
	var RoleFactory = {};

	RoleFactory.saveRoles = function(data,cb){
		$http.post('/api/1.0/roles/',data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	RoleFactory.getAllRoles = function(cb){
		$http.get('/api/1.0/roles/').success(function(data){
			cb(data);
			console.log(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	RoleFactory.getSpRole = function(id,cb){
		$http.get('/api/1.0/roles/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	RoleFactory.deleteRole = function(id,cb){
		$http.delete('/api/1.0/user/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};



	return RoleFactory;
}
var LogFactory = function($http){
	var LogFactory = {};

	LogFactory.saveLogs = function(id,data,cb){
		$http.post('/api/1.0/log/'+id,data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	LogFactory.getuserLogs = function(id,cb){
		$http.get('/api/1.0/log/'+id).success(function(data){
			cb(data);
			console.log('callback response: userlogs:',data+id);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	LogFactory.getSpLogs = function(id,cb){
		$http.get('/api/1.0/roles/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};


	return LogFactory;
}
var RequestFactory = function($http){
	RequestFactory.getuserrequest= function(id,cb){
		
		$http.get('/api/1.0/request/'+id+'/NO').success(function(data){
			cb(data);
			console.log(data);
		}).error(function(err){
			console.log('error ...',err);
		});
	};
	RequestFactory.saverequest=function(id,data,cb){
$http.post('/api/1.0/request/'+id,data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});

	}
	RequestFactory.getSpRequest=function(id,cb){
		$http.get('/api/1.0/requestSP/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});

	}


	return RequestFactory;
}
var AddressFactory = function($http){
	var AddressFactory = {};

	AddressFactory.saveCountry = function(data,cb){
		$http.post('/api/1.0/country',data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};
	AddressFactory.saveState= function(id,data,cb){
		$http.post('/api/1.0/state/'+id,data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};
	AddressFactory.saveLocation= function(id,id1,data,cb){
		$http.post('/api/1.0/location/'+id+'/'+id1,data).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};
	AddressFactory.getStates=function(cb){
		$http.get('/api/1.0/state/').success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});

	};
	AddressFactory.getLocations=function(cb){
		$http.get('/api/1.0/location/').success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});

	};

	AddressFactory.getAllAddress = function(cb){
		$http.get('/api/1.0/address').success(function(data){
			cb(data);
			console.log(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	AddressFactory.getAllCountries= function(cb){
		$http.get('/api/1.0/country').success(function(data){
			cb(data);
		}).error(function(err){
			console.log('error ...',err);
		});
	};

	AddressFactory.getAllStatebyCountryID= function(id,cb){
		console.log('getAllstate ID ...',id);
		$http.get('/api/1.0/state/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('error ...',err);
		});
	};
	AddressFactory.getAlllocationbystate= function(id,cb){
		$http.get('/api/1.0/location/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('error ...',err);
		});

	};

	AddressFactory.getSpAddress = function(id,cb){
		$http.get('/api/1.0/address/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	AddressFactory.deleteAddress = function(id,cb){
		$http.delete('/api/1.0/address/'+id).success(function(data){
			cb(data);
		}).error(function(err){
			console.log('Error ...',err);
		});
	};

	return AddressFactory;
}
nodeMongo.Services.factory('AddressFactory',['$http' ,AddressFactory]);
nodeMongo.Services.factory('RoleFactory',['$http' ,RoleFactory]);
nodeMongo.Services.factory('UserFactory',['$http' ,UserFactory]);
nodeMongo.Services.factory('LogFactory',['$http' ,LogFactory]);
nodeMongo.Services.factory('RequestFactory',['$http' ,RequestFactory]);

