'use strict';

module.exports = function(mongoose){
	var locationSchema = new mongoose.Schema({
		location_name: String,
		countryID:{
			type: mongoose.Schema.Types.ObjectId,
            ref: 'country'
		},
		stateID:{
			type: mongoose.Schema.Types.ObjectId,
            ref: 'state'
		}
	});

	mongoose.model('location', locationSchema);
}