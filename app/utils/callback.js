'use strict';

exports.setupResponseCallback = function (res) {

    return function (error, returnValue) {
        if (error) {
            //console.error(error);
             //res.json(500, error);
            return res.status(500).json(error);
        }

        res.status(200).json(returnValue);
    };
}

exports.requestErrorHandler = function(callback) {

    return function (err, resp) {
        if(err) {
            console.log('Error', err);
            return callback(err, {})
        }
        console.log('Response from API ----- ', resp.body);
        return callback(null, resp.body);
    }
};
