'use strict';

module.exports = function(mongoose){
	var RoleSchema = new mongoose.Schema({
		RoleDesc:String,
		Read:Number,
		Write:Number,
		Delete:Number

	});
	mongoose.model('Roles', RoleSchema);
};
