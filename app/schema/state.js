'use strict';

module.exports = function(mongoose){
	var stateSchema = new mongoose.Schema({
		s_name: String,
		s_code: String,
		countryID:{
			type: mongoose.Schema.Types.ObjectId,
            ref: 'country'
		}
	});

	mongoose.model('state', stateSchema);
}