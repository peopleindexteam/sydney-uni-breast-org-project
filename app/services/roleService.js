'use strict';


var roleDao = require('../daos/roleDao');

 function Role() {
    this.roleDao = roleDao;
 }

 Role.prototype.saverole = function(data, next) {
     roleDao.saverole(data, next);
 };
 Role.prototype.getallrole= function(next){
     roleDao.getallrole(next);
 };
 Role.prototype.findrole = function findUser(id,next){
     roleDao.findrole(id,next);
 };

 Role.prototype.updaterole = function(id,data,next) {
 	roleDao.updaterole(id,data,next)
 };

 Role.prototype.deleteRolesById = function(id, next){
 	roleDao.deleteRolesById(id, next);
 }

exports.Role = Role;
