'use strict';

var mongoose = require('mongoose');
var User= mongoose.model('User_Account');
var UserProfile = mongoose.model('User_Profile');
var Request = mongoose.model('Request');
var async = require('async');

// Save data
exports.saverequest = function saverequest(_id,data, next) {
        async.waterfall([
            function(callback){
                UserProfile.findOne({userID: _id},function(err, result){
                    if(err){ throw err;}
                    callback(null,result);
                })
            },
        	function(result,callback){
                if(result === null){
                    callback({return: true},null);
                }else{
                    data['userprofileID'] = result._id;
                    data['userID'] = result.userID;
                    data['countryID']=result.countryID;
                    data['locationID']=result.locationID;
                    data['stateID']=result.stateID;
                    data['approve_tag'] = "NO";
                    Request.create(data,function(err, result){
                        if(err){ throw err;}
                        callback(null, result);
                    });
                }
        	}
        ],next)
};

exports.updaterequest = function updaterequest(id,data, next) {
    console.log('ready for Update:',JSON.stringify(data));
    Request.findByIdAndUpdate(id,data, next);
    };
exports.updaterequestmore= function updatemore(id,data,next){
    console.log('ready to update:',data);
    Request.findByIdAndUpdate(id,{$push: data},{safe: true, upsert: true},next);
    };

exports.findrequestdetails = function findrequestdetails(id,appstat, next) {
   	Request
   		.find({userID: id,approve_tag:appstat},next)
        .populate('userID','u_email u_isverified')
        .populate('userprofileID','u_fname u_lname u_image')
        .populate('stateID')
        .populate('locationID')
        .populate('countryID')
   		.exec(function(err, data){
   			if(err) return handleError(err);
   		});
};
exports.getsprequest= function getsprequest(id,next){

    Request.
    find({_id:id},next)
    .populate('userprofileID')
    .exec(function(err,data){
        if(err) return handleError(err);
    });
}

exports.getallrequest = function getallrequest(appstat,next){
    Request
        .find({approve_tag: appstat},next)
        .populate('userID','u_email u_isverified')
        .populate('userprofileID')
        .populate('stateID')
        .populate('locationID')
        .populate('countryID')
        .exec(function(err, data){
            if(err) return handleError(err);
        });
};
exports.deleteRequestById = function deleteRequestById(id,next){
    Request.findByIdAndRemove(id,next);
}

exports.deletespecificupload = function deletespecificupload(reqID,obID,reqfile,next){
   if(reqfile==='test_sets'){
     Request.update({'_id': reqID},  { $pull: { "test_sets": { _id:obID } } },next);
   }
   if(reqfile==='images'){
     Request.update({'_id': reqID},  { $pull: { "images": { _id:obID } } },next);
    }
   if(reqfile==='reading'){
     Request.update({'_id': reqID},  { $pull: { "reading": { _id:obID } } },next);
    }
   if(reqfile==='results'){
     Request.update({'_id': reqID},  { $pull: { "results": { _id:obID } } },next);
    }
}

exports.deleteRequest = function deleteRequest(id, next){
    Request.findByIdAndRemove({_id: id}, next);
}

