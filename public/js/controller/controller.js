'use strict';

var ImageCtrl = function($scope, $upload){
	$scope.$watch('files', function () {

        $scope.upload($scope.files);
        
    });
//upload user Image
        $scope.upload = function (id,files) {
            
            if (files && files.length) {
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    $upload.upload({
                        url: '/api/1.0/user/uploadImage/'+id,
                        file: file,
                        method: 'PUT'
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);

                    }).success(function (data, status, headers, config) {
                       
                        console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    });
                }
            }
        };
    }

var UserCtrl = function($scope,$location,$q,UserFactory,RoleFactory,AddressFactory){

    var data = [{
        file_name: 'image',
        file_type: 'jpeg',
        url: 'uploads/img',
        file_size: '12345'
    },{
        file_name: 'image',
        file_type: 'jpeg',
        url: 'uploads/img',
        file_size: '12345'
    }];

    $scope.NewUser = {};
    $scope.documents = {};
    $scope.documents.d_file = [];
    $scope.test = [];
    $scope.test_sets = {};
    $scope.test_sets.file_name = 'image';
    $scope.test_sets.file_type = 'jpeg';
    $scope.test_sets.url = 'uploads/img';
    $scope.test_sets.file_size = '12345';

    $scope.test.push(data);

    $scope.documents.d_file.push({test_sets: data});
    $scope.documents.purpose = 'Purpose Test';
    $scope.documents.approve_date = '12-12-12';
    $scope.documents.valid_until = '12-01-19';
    $scope.documents.filters = 'Sample Filter';

    $scope.documents.addressID = '';

    UserFactory.getAllUser(function(data){
       $scope.AllUser = data;

    })

    RoleFactory.getAllRoles(function(data){
       $scope.AllRoles = data;
    })

    AddressFactory.getAllCountries(function(data){
        $scope.AllCountries=data;
    })

     AddressFactory.getStates(function(data){
        $scope.AllStatebyCountry=data;
    })

    AddressFactory.getLocations(function(data){
        $scope.Alllocationbystate=data;
    })

    $scope.getState=function(){
    AddressFactory.getAllStatebyCountryID($scope.NewUser.countryID,function(data){
        $scope.AllStatebyCountry=data;
    })
    }
    $scope.getStated=function(){
    AddressFactory.getAllStatebyCountryID($scope.TheUser.countryID,function(data){
        $scope.AllStatebyCountry=data;
    })
    }

    $scope.getlocation=function(){
    AddressFactory.getAlllocationbystate($scope.NewUser.stateID,function(data){
        $scope.Alllocationbystate=data;
    })
    }

     $scope.getlocations=function(){
    AddressFactory.getAlllocationbystate($scope.TheUser.stateID,function(data){
        $scope.Alllocationbystate=data;
    })
    }

     $scope.getuserupdate= function(id){
        var data={};
            data['countryID']=$scope.TheUser.countryID;
            data['locationID']=$scope.TheUser.locationID;
            data['organization']=$scope.TheUser.organization;
            data['roleID']=$scope.TheUser.roleID;
            data['stateID']=$scope.TheUser.stateID;
            data['u_email']=$scope.TheUser.u_email;
            data['u_fname']=$scope.TheUser.u_fname;
            data['u_lname']=$scope.TheUser.u_lname;
            data['u_password']=$scope.TheUser.u_password;
            console.log('the data:',data);
       UserFactory.updateUser(id,data,function(data){
            if(data){
                UserFactory.getAllUser(function(data){
                    console.log(data);
                   $scope.AllUser = data;
                })
            }
       })
    }

    $scope.getUser = function(){
       UserFactory.saveUser($scope.NewUser,function(data){
            if(data){
                UserFactory.getAllUser(function(data){
                    console.log(data);
                   $scope.AllUser = data;
                })
            }
       })
    }

    $scope.getSpUser = function(id){
        UserFactory.getSpUser(id,function(data){
            $scope.TheUser = data;

        })
    }

    $scope.deleteUser = function(index,id){
        var r = confirm("Are you sure to delete this User!!!...");
        if (r == true) {
          UserFactory.deleteUser(id,function(data){
            $scope.AllUser.splice(index,1)       
        })
        } 
    }

    $scope.saveDocuments = function(){
       console.log($scope.documents);
        UserFactory.saveDocuments($scope.documents,function(callback){
           console.log(callback);
        })

        }

    }
var RoleCtrl = function($scope,RoleFactory){

    RoleFactory.getAllRoles(function(data){
       $scope.AllRoles = data;
    })

    $scope.getRoles = function(){
       RoleFactory.saveRoles($scope.NewRole,function(data){
            if(data){
                RoleFactory.getAllRoles(function(data){
                    console.log(data);
                   $scope.AllRoles = data;
                })
            }
       })
    }

    $scope.getSpRole = function(id){
        RoleFactory.getSpRole(id,function(data){
            $scope.NewRole = data;
        })
    }

    $scope.deleteRole = function(index,id){
        RoleFactory.deleteRole(id,function(data){
            $scope.AllRoles.splice(index,1)
        })
        }
    }
var RequestCtrl = function($scope,RequestFactory,UserFactory){
        UserFactory.getAllUser(function(data){
       $scope.AllUser = data;

             })
          $scope.getuserrequests = function(){
    console.log("user ID :",$scope.NewRequest.userID);
    RequestFactory.getuserrequest($scope.NewRequest.userID,function(data){
       $scope.UserRequest = data;
            })
            }
    $scope.getRequest= function(){
                RequestFactory.saverequest($scope.NewRequest.userID,$scope.NewRequest,function(data){
                    if(data){
                         RequestFactory.getuserrequest($scope.NewRequest.userID,function(data){
                    $scope.UserRequest = data;
                      })

                 }

            })
         }
         $scope.getSpRequest = function(id){
        RequestFactory.getSpRequest(id,function(data){
            $scope.sprequest = data[0];
            console.log('Specific Request are:',$scope.sprequest);
        })
    }
 }

var LogCtrl = function($scope,LogFactory,UserFactory){
    $scope.NewLog= {};
    $scope.NewLog.logTranType=[{desc: ''}];
    $scope.NewLog.page =[{name: ''}];

    $scope.test = function(){
        console.log($scope.NewLog);
    }

    UserFactory.getAllUser(function(data){
       $scope.AllUser = data;

    })

    $scope.getuserLogs = function(){

    LogFactory.getuserLogs($scope.NewLog.userID,function(data){
       $scope.UserLogs = data;
    })
}

    $scope.getLogs = function(){
        console.log($scope.NewLog)
       LogFactory.saveLogs($scope.NewLog.userID,$scope.NewLog,function(data){
            if(data){
                LogFactory.getuserLogs($scope.NewLog.userID,function(data){
                   $scope.UserLogs = data;
                })
            }
       })
    }

    $scope.getSpLogs = function(id){
        LogFactory.getSpLogs(id,function(data){
            $scope.NewLog = data;
        })
    }
}

var AddressCtrl = function($scope,AddressFactory){
  // $scope.NewAddress= {};
  // $scope.NewAddress.state=[{name:''}];
  //  $scope.NewAddress.location=[{name:''}];
    AddressFactory.getAllAddress(function(data){
       $scope.AllAddress= data;
       
    })

    AddressFactory.getAllCountries(function(data){
        $scope.AllCountries=data;
    })
    //  AddressFactory.getAllStatebyCountryID(function(data){
    //     $scope.AllStatebyCountry=data;
    // })
    AddressFactory.getStates(function(data){

    })
    AddressFactory.getLocations(function(data){

    })

    $scope.getAddress = function(){
       AddressFactory.saveAddress($scope.NewAddress,function(data){

            if(data){
                AddressFactory.getAllAddress(function(data){
                    console.log(data);
                   $scope.AllAddress = data;
                })
            }
       })
    }

     $scope.getCountry = function(){

            console.log($scope.NewAddress);
       AddressFactory.saveCountry($scope.NewCountry,function(data){

            if(data){
                AddressFactory.getAllCountries(function(data){
        $scope.AllCountries=data;
                          })
            }
       })
    }

    $scope.getAllstate= function(){

       AddressFactory.saveState($scope.NewState.countryID,$scope.NewState,function(data){

            if(data){
                AddressFactory.getAllStatebyCountryID($scope.NewState.countryID,function(data){
                 $scope.AllStatebyCountry=data;
                          })
            }
       })
    }
    $scope.getalllocation= function(){

       AddressFactory.saveLocation($scope.NewLocation.countryID,$scope.NewLocation.stateID,$scope.NewLocation,function(data){

            if(data){
               AddressFactory.getAllAddress(function(data){
                $scope.AllAddress= data;
                 })
            }
       })
    }

    $scope.getState=function(){
    AddressFactory.getAllStatebyCountryID($scope.NewLocation.countryID,function(data){
        $scope.AllStatebyCountry=data;
        console.log($scope.AllStatebyCountry);
    })
    }
   
    $scope.getSpAddress = function(id){
        AddressFactory.getSpAddress(id,function(data){
            $scope.NewAddress = data;
        })
    }

    $scope.deleteAddress = function(index,id){
        AddressFactory.deleteAddress(id,function(data){
            $scope.AllAddress.splice(index,1)
        })
    }
}

nodeMongo.Controllers.controller('ImageCtrl', ['$scope', '$upload', ImageCtrl]);
nodeMongo.Controllers.controller('UserCtrl', ['$scope','$location','$q','UserFactory','RoleFactory','AddressFactory', UserCtrl]);
nodeMongo.Controllers.controller('RoleCtrl', ['$scope','RoleFactory', RoleCtrl]);
nodeMongo.Controllers.controller('AddressCtrl', ['$scope','AddressFactory', AddressCtrl]);
nodeMongo.Controllers.controller('LogCtrl', ['$scope','LogFactory','UserFactory', LogCtrl]);
nodeMongo.Controllers.controller('RequestCtrl', ['$scope','RequestFactory','UserFactory', RequestCtrl]);

